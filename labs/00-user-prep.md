# Welcome to the course!

For authentication to kubernetes, you will use a "kubeconf" file. This is normally located in your user's home dir under .kube. Like everything else in kubernetes, it's in yaml format. 
It consists of several fields, including a client certificate for authentication as well as a server URL to talk to the kubernetes API. 

One way of simplifying authentication is to merely export the KUBECONFIG variable and have it point to the file. To do this, run:

```
export KUBECONFIG=/home/<username>/.kube/config_sudo-lab.yaml
```

To interact with a cluster, every command always starts with 'kubectl'. With kubectl, you can do a lot of things - create deployments, change cluster settings, check your pod's status and so on. Generally the syntax is like so:

```
kubectl <verb> <resource> <arguments>
```



You will do some exercises, but in order to not collide with your colleagues, you will need to do these in your own namespace. In order to create your namespace, run:

```
kubectl create ns <username>
```


To see all namespaces in the clusters run:

```
kubectl get ns
```


To change your context or 'position' inside the cluster, you can also use kubectl. However, to do this in a less cumbersome way, instead use the command 'kubens'.

```
kubens <username>
```

